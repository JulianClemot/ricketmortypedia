package com.julian.ricketmortypedia.common.model.characterslist

import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status

data class CharacterLight(
    val id: String,
    val name: String,
    val gender: Gender,
    val status: Status,
    val avatarUrl: String?
)