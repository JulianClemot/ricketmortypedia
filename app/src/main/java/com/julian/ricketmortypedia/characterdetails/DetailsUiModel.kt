package com.julian.ricketmortypedia.characterdetails

import com.julian.ricketmortypedia.common.base.BaseUiModel

data class DetailsUiModel(
    val selectedColor: Int,
    val name: String,
    val status: String,
    val gender: String,
    val avatarUrl: String?,
    val species: String,
    val type: String,
    val locationName: String,
    val episodeNameList: List<String>
) : BaseUiModel()