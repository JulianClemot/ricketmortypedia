package com.julian.ricketmortypedia.common.model.converters

import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.common.model.characterslist.CharactersListResult
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.ricketmortyapi.GetCharactersQuery


fun GetCharactersQuery.Characters?.toModel() = CharactersListResult(
    this?.info?.prev,
    this?.info?.next,
    this?.results?.map { it.toModel() }.orEmpty(),
)

fun GetCharactersQuery.Result?.toModel() =
    CharacterLight(
        this?.id.orEmpty(),
        this?.name.orEmpty(),
        toGenderEnum(this?.gender),
        toStatusEnum(this?.status),
        this?.image
    )

fun toGenderEnum(gender: String?) =
    Gender.values().firstOrNull { it.name.equals(gender, ignoreCase = true) } ?: Gender.UNKNOWN

fun toStatusEnum(status: String?) =
    Status.values().firstOrNull { it.name.equals(status, ignoreCase = true) } ?: Status.UNKNOWN