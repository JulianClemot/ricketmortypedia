package com.julian.ricketmortypedia.common.model

enum class Gender {
    MALE,
    FEMALE,
    GENDERLESS,
    UNKNOWN,
}