package com.julian.ricketmortypedia.common.model.characterdetails

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Location(
    val id: String,
    val name: String,
    val type: String,
    val dimension: String
) : Parcelable
