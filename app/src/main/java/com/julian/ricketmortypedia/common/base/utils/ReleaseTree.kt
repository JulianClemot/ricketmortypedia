package com.julian.ricketmortypedia.common.base.utils

import timber.log.Timber

class ReleaseTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, throwable: Throwable?) {
        //We leave this empty to prevent any log to show up
    }
}
