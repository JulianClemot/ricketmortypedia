package com.julian.ricketmortypedia.common.model.characterdetails

import android.os.Parcelable
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import kotlinx.parcelize.Parcelize

@Parcelize
data class CharacterFull(
    val id: String,
    val name: String,
    val gender: Gender,
    val status: Status,
    val avatarUrl: String?,
    val species: String,
    val type: String,
    val location: Location?,
    val episodes: List<Episode>
) : Parcelable