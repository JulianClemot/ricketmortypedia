package com.julian.ricketmortypedia.home

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingData
import com.julian.ricketmortypedia.common.base.AppError
import com.julian.ricketmortypedia.common.base.Result
import com.julian.ricketmortypedia.common.base.UiState
import com.julian.ricketmortypedia.common.data.pagination.CharactersListPagination
import com.julian.ricketmortypedia.common.data.repository.RickAndMortyRepository
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterDetailsResult
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull
import com.julian.ricketmortypedia.common.model.characterdetails.Location
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.utils.MainCoroutineScopeRule
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class HomeViewModelTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineScopeRule = MainCoroutineScopeRule()

    @MockK(relaxed = true)
    private lateinit var charactersListPagination: CharactersListPagination

    @MockK(relaxed = true)
    private lateinit var repository: RickAndMortyRepository

    private lateinit var viewModel: HomeViewModel

    @Before
    fun before() {
        MockKAnnotations.init(this)
        viewModel =
            HomeViewModel(
                repository,
                mainCoroutineScopeRule.dispatcher
            )
        every { repository.charactersListPagination } returns charactersListPagination
    }

    @Test
    fun `test loadCharacters with a good repo result should get a success uiModel`() {
        mainCoroutineScopeRule.runBlockingTest {
            coEvery {
                repository.loadCharacters(
                    any(), any()
                )
            } returns flowOf(
                PagingData.from(buildCharactersLightList())
            )

            viewModel.loadCharacters()

            val result = viewModel.uiState.first()
            Assert.assertTrue(result is UiState.Success)
            val uiModel = (result as UiState.Success).uiModel
            Assert.assertTrue(uiModel is HomeUiModel.CharactersListModel)
        }
    }

    @Test
    fun `test processGenderSelection with a good repo result should get a success uiModel`() {
        mainCoroutineScopeRule.runBlockingTest {
            coEvery {
                repository.loadCharacters(
                    any(), any()
                )
            } returns flowOf(
                PagingData.from(buildCharactersLightList())
            )

            viewModel.processGenderSelection(null)

            val result = viewModel.uiState.first()
            Assert.assertTrue(result is UiState.Success)
            val uiModel = (result as UiState.Success).uiModel
            Assert.assertTrue(uiModel is HomeUiModel.CharactersListModel)
        }
    }

    @Test
    fun `test processStatusSelection with a good repo result should get a success uiModel`() {
        mainCoroutineScopeRule.runBlockingTest {
            coEvery {
                repository.loadCharacters(
                    any(), any()
                )
            } returns flowOf(
                PagingData.from(buildCharactersLightList())
            )

            viewModel.processStatusSelection(null)

            val result = viewModel.uiState.first()
            Assert.assertTrue(result is UiState.Success)
            val uiModel = (result as UiState.Success).uiModel
            Assert.assertTrue(uiModel is HomeUiModel.CharactersListModel)
        }
    }

    @Test
    fun `test processCharacterSelected with a non null characterLight should send success uiModel`() {
        val view = mockk<View>()
        val color = 1
        val characterLight = CharacterLight("1", "", Gender.FEMALE, Status.ALIVE, "")
        val characterFull = CharacterFull(
            "1", "", Gender.FEMALE, Status.ALIVE, "", "", "",
            Location("", "", "", ""), listOf()
        )

        coEvery { repository.getCharacterDetails(any()) } returns Result.Success(
            CharacterDetailsResult(characterFull)
        )
        mainCoroutineScopeRule.runBlockingTest {
            viewModel.processCharacterSelected(view, color, characterLight)

            val result = viewModel.uiState.first()

            Assert.assertTrue(result is UiState.Success)
            val success = result as UiState.Success

            Assert.assertTrue(success.uiModel is HomeUiModel.DisplayCharacterDetailsModel)
            val uiModel = result.uiModel as HomeUiModel.DisplayCharacterDetailsModel
            val retrievedCharacter = uiModel.characterFull

            Assert.assertEquals(characterFull.id, retrievedCharacter.id)
        }
    }

    @Test
    fun `test processCharacterSelected with a null result from api should send error event`() {
        val view = mockk<View>()
        val color = 1
        val appError = AppError("erreur")
        val characterLight = CharacterLight("1", "", Gender.FEMALE, Status.ALIVE, "")

        coEvery { repository.getCharacterDetails(any()) } returns Result.Error(
            appError
        )
        mainCoroutineScopeRule.runBlockingTest {
            viewModel.processCharacterSelected(view, color, characterLight)

            val result = viewModel.uiState.first()

            Assert.assertTrue(result is UiState.Error)
            Assert.assertEquals(appError.message, (result as UiState.Error).error.message)
        }
    }

    @Test
    fun `test processCharacterSelected with a null characterLight should send error event`() {
        val view = mockk<View>()
        val color = 1

        val characterFull = CharacterFull(
            "1", "", Gender.FEMALE, Status.ALIVE, "", "", "",
            Location("", "", "", ""), listOf()
        )

        coEvery { repository.getCharacterDetails(any()) } returns Result.Success(
            CharacterDetailsResult(characterFull)
        )
        mainCoroutineScopeRule.runBlockingTest {
            viewModel.processCharacterSelected(view, color, null)

            val result = viewModel.uiState.first()

            Assert.assertTrue(result is UiState.Error)
        }
    }

    private fun buildCharactersLightList() = listOf(
        CharacterLight("1", "", Gender.FEMALE, Status.ALIVE, ""),
        CharacterLight("2", "", Gender.FEMALE, Status.ALIVE, ""),
    )
}