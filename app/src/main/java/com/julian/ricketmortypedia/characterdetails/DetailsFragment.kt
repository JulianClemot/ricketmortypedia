package com.julian.ricketmortypedia.characterdetails

import android.os.Bundle
import android.transition.TransitionInflater
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.julian.ricketmortypedia.R
import com.julian.ricketmortypedia.characterdetails.helpers.EpisodesAdapter
import com.julian.ricketmortypedia.common.application.GlideApp
import com.julian.ricketmortypedia.common.base.AppError
import com.julian.ricketmortypedia.common.base.BaseFragment
import com.julian.ricketmortypedia.common.base.ext.hide
import com.julian.ricketmortypedia.databinding.FragmentDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsFragment :
    BaseFragment<DetailsViewModel, FragmentDetailsBinding, DetailsUiModel>(R.layout.fragment_details) {

    override val viewModel: DetailsViewModel by viewModels()

    private val args: DetailsFragmentArgs by navArgs()

    private val episodesAdapter by lazy { EpisodesAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)
        sharedElementEnterTransition = transition
        sharedElementReturnTransition = transition


    }

    override fun onInit() {
        setActionBarColor(args.selectedColor)

        viewModel.character = args.character
        viewModel.selectedColor = args.selectedColor
        viewModel.loadCharacter()

        binding.episodesRecyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = episodesAdapter
        }
        binding.episodesRecyclerView.isNestedScrollingEnabled = false
    }

    override fun onError(error: AppError) {
        handleError(error)
    }


    override fun onSuccess(uiModel: DetailsUiModel) {
        with(uiModel) {
            binding.name.text = name
            binding.name.setTextColor(selectedColor)

            GlideApp.with(requireContext())
                .load(avatarUrl)
                .circleCrop()
                .into(binding.avatar)

            setTextOrHide(binding.gender, gender, getString(R.string.gender_label))
            setTextOrHide(binding.species, species, getString(R.string.species_label))
            setTextOrHide(binding.status, status, getString(R.string.status_label))
            setTextOrHide(binding.type, type, getString(R.string.type_label))
            setTextOrHide(binding.location, locationName, getString(R.string.location_label))

            episodesAdapter.items = episodeNameList.toMutableList()
        }
    }

    private fun setTextOrHide(textView: TextView, valueToDisplay: String, stringToFormat: String) {
        if (valueToDisplay.isNotEmpty()) {
            textView.text = String.format(stringToFormat, valueToDisplay)
        } else {
            textView.hide()
        }
    }
}