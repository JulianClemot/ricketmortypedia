package com.julian.ricketmortypedia.home

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.julian.ricketmortypedia.R
import com.julian.ricketmortypedia.common.base.AppError
import com.julian.ricketmortypedia.common.base.BaseFragment
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.databinding.FragmentHomeBinding
import com.julian.ricketmortypedia.home.helpers.CharactersAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HomeFragment :
    BaseFragment<HomeViewModel, FragmentHomeBinding, HomeUiModel>(R.layout.fragment_home) {

    companion object {
        const val LIST_ROWS = 3
    }

    override val viewModel: HomeViewModel by viewModels()

    private val charactersAdapter by lazy {
        CharactersAdapter { avatarView, selectedColor, character ->
            viewModel.processCharacterSelected(avatarView, selectedColor, character)
        }
    }

    private var filtersBottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>? = null

    override fun onInit() {
        setActionBarColor(ContextCompat.getColor(requireContext(), R.color.primaryColor))
        initCharacters()
        initFilters()
    }

    override fun onError(error: AppError) {
        binding.progressIndicator.hide()
        handleError(error)
    }

    override fun onLoading(progress: Int?) {
        binding.progressIndicator.show()
    }

    override fun onSuccess(uiModel: HomeUiModel) {
        when (uiModel) {
            is HomeUiModel.CharactersListModel -> setCharactersList(uiModel.charactersListPaginated)
            is HomeUiModel.DisplayCharacterDetailsModel -> goToCharacterDetailsPage(
                uiModel.avatarView,
                uiModel.selectedColor,
                uiModel.characterFull
            )
        }
    }

    private fun goToCharacterDetailsPage(
        avatarView: View,
        selectedColor: Int,
        characterFull: CharacterFull
    ) {
        val extras = FragmentNavigatorExtras(
            avatarView to getString(R.string.transition_avatar_to_details)
        )
        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToDetailsFragment(
                characterFull, selectedColor
            ), extras
        )
    }

    private fun setCharactersList(charactersListPaginated: PagingData<CharacterLight>) {
        lifecycleScope.launch {
            binding.progressIndicator.hide()
            charactersAdapter.submitData(charactersListPaginated)
        }
    }

    private fun initCharacters() {
        viewModel.loadCharacters()
        binding.charactersList.apply {
            adapter = charactersAdapter
            layoutManager = GridLayoutManager(context, LIST_ROWS)
        }
    }

    private fun initFilters() {
        binding.filtersBtn.setOnClickListener {
            filtersBottomSheetBehavior?.state =
                if (filtersBottomSheetBehavior?.state == BottomSheetBehavior.STATE_EXPANDED) BottomSheetBehavior.STATE_HIDDEN else BottomSheetBehavior.STATE_EXPANDED
        }

        filtersBottomSheetBehavior = BottomSheetBehavior.from(binding.filters.filtersBottomSheet)
        filtersBottomSheetBehavior?.apply {
            addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    //nothing to do
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    binding.filtersBtn.setImageResource(
                        when (newState) {
                            BottomSheetBehavior.STATE_EXPANDED -> R.drawable.ic_close
                            else -> R.drawable.ic_filters
                        }
                    )
                }
            })

            state = BottomSheetBehavior.STATE_HIDDEN
        }

        initGenderFilters()
        initStatusFilters()
    }

    private fun initStatusFilters() {
        //we selected "all" filters by default
        binding.filters.apply {
            statusChipAll.isChecked = true
            binding.filters.statusChipsGroup.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    statusChipAlive.id -> viewModel.processStatusSelection(Status.ALIVE)
                    statusChipDead.id -> viewModel.processStatusSelection(Status.DEAD)
                    statusChipUnknown.id -> viewModel.processStatusSelection(Status.UNKNOWN)
                    else -> viewModel.processStatusSelection(null)
                }
            }
        }
    }

    private fun initGenderFilters() {
        //we selected "all" filters by default
        binding.filters.apply {
            genderChipAll.isChecked = true

            binding.filters.genderChipsGroup.setOnCheckedChangeListener { _, checkedId ->
                when (checkedId) {
                    genderChipMale.id -> viewModel.processGenderSelection(Gender.MALE)
                    genderChipFemale.id -> viewModel.processGenderSelection(Gender.FEMALE)
                    genderChipGenderless.id -> viewModel.processGenderSelection(Gender.GENDERLESS)
                    genderChipUnknown.id -> viewModel.processGenderSelection(Gender.UNKNOWN)
                    else -> viewModel.processGenderSelection(null)
                }
            }
        }
    }
}