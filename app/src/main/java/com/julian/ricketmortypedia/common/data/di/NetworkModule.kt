package com.julian.ricketmortypedia.common.data.di

import android.content.Context
import com.apollographql.apollo.ApolloClient
import com.julian.ricketmortypedia.BuildConfig
import com.julian.ricketmortypedia.R
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providesLoggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    @Singleton
    @Provides
    fun providesOkHttp(httpLoggingInterceptor: HttpLoggingInterceptor) = OkHttpClient.Builder()
        .addInterceptor(httpLoggingInterceptor)
        .build()

    @Singleton
    @Provides
    fun providesApollo(okHttpClient: OkHttpClient, @ApplicationContext app: Context) : ApolloClient = ApolloClient.builder()
            .serverUrl(app.getString(R.string.base_url))
            .okHttpClient(okHttpClient)
            .build()
}