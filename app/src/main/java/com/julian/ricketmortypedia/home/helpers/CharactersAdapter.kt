package com.julian.ricketmortypedia.home.helpers

import android.graphics.Bitmap
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.paging.PagingDataAdapter
import androidx.palette.graphics.Palette
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.julian.ricketmortypedia.R
import com.julian.ricketmortypedia.common.application.GlideApp
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.databinding.ItemCharactersBinding
import java.util.*


class CharactersAdapter(private var onClickListener: (sharedElements: View,selectedColor : Int, character: CharacterLight?) -> Unit = { _,_, _ -> }) :
    PagingDataAdapter<CharacterLight, CharactersAdapter.CharacterViewHolder>(CharacterDiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        return CharacterViewHolder(
            ItemCharactersBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class CharacterViewHolder(
        private val binding: ItemCharactersBinding,
        private val onClickListener: (sharedElements: View,selectedColor : Int, character: CharacterLight?) -> Unit? = { _,_, _ -> }
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(character: CharacterLight?) {
            with(binding) {
                container.setOnClickListener {
                    ViewCompat.setTransitionName(
                        avatar,
                        itemView.context.getString(R.string.transition_avatar_to_details)
                    )
                    onClickListener.invoke(avatar,(viewBackground.background as ColorDrawable).color, character)
                }

                name.text = character?.name
                gender.text = character?.gender?.name.orEmpty().toLowerCase(Locale.getDefault())
                    .capitalize(Locale.getDefault())
                status.text = character?.status?.name.orEmpty().toLowerCase(Locale.getDefault())
                    .capitalize(Locale.getDefault())

                GlideApp.with(itemView.context)
                    .asBitmap()
                    .load(character?.avatarUrl)
                    .circleCrop()
                    .listener(object : RequestListener<Bitmap?> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Bitmap?>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Bitmap?,
                            model: Any?,
                            target: Target<Bitmap?>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            if (resource != null) {
                                val palette = Palette.from(resource).generate()
                                setContentColorsFromPalette(palette)
                            }
                            return false
                        }

                    }).into(avatar)
            }
        }

        private fun setContentColorsFromPalette(palette: Palette) {
            val defaultColor =
                ContextCompat.getColor(itemView.context, R.color.black)
            val lightVibrantColor = palette.getLightVibrantColor(defaultColor)
            with(binding) {
                name.setTextColor(lightVibrantColor)
                status.setTextColor(lightVibrantColor)
                gender.setTextColor(lightVibrantColor)
                val defaultBackground =
                    ContextCompat.getColor(itemView.context, R.color.white)
                viewBackground.setBackgroundColor(
                    palette.getDarkMutedColor(
                        defaultBackground
                    )
                )
            }
        }
    }
}

class CharacterDiffUtils : DiffUtil.ItemCallback<CharacterLight>() {
    override fun areItemsTheSame(oldItem: CharacterLight, newItem: CharacterLight) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: CharacterLight, newItem: CharacterLight) =
        oldItem.id == newItem.id && oldItem.name == newItem.name
}
