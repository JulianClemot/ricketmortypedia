package com.julian.ricketmortypedia.home

import android.view.View
import androidx.lifecycle.viewModelScope
import androidx.paging.cachedIn
import com.julian.ricketmortypedia.common.base.AppError
import com.julian.ricketmortypedia.common.base.BaseViewModel
import com.julian.ricketmortypedia.common.base.Result
import com.julian.ricketmortypedia.common.data.repository.RickAndMortyRepository
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: RickAndMortyRepository,
    private val dispatcher: CoroutineDispatcher,
) : BaseViewModel<HomeUiModel>() {

    fun loadCharacters() {
        viewModelScope.launch(dispatcher) {
            repository.loadCharacters().cachedIn(viewModelScope).collect { pagingData ->
                success(HomeUiModel.CharactersListModel(pagingData))
            }
        }
    }

    fun processGenderSelection(gender: Gender?) {
        viewModelScope.launch(dispatcher) {
            repository.loadCharacters(filterGender = gender).cachedIn(viewModelScope).onStart {
                loading()
            }.collect { pagingData ->
                success(HomeUiModel.CharactersListModel(pagingData))
            }
        }
    }

    fun processStatusSelection(status: Status?) {
        viewModelScope.launch(dispatcher) {
            repository.loadCharacters(filterStatus = status).cachedIn(viewModelScope).onStart {
                loading()
            }.collect { pagingData ->
                success(HomeUiModel.CharactersListModel(pagingData))
            }
        }
    }

    fun processCharacterSelected(
        avatarView: View,
        selectedColor: Int,
        characterLight: CharacterLight?
    ) {
        viewModelScope.launch(dispatcher) {
            characterLight?.let { character ->
                when (val result = repository.getCharacterDetails(character.id)) {
                    is Result.Error -> error(result.error)
                    is Result.Success -> success(
                        HomeUiModel.DisplayCharacterDetailsModel(
                            avatarView,
                            selectedColor,
                            result.data.character
                        )
                    )
                }
            } ?: run {
                error(AppError("Character is non-existent"))
            }
        }
    }
}