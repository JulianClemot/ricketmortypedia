package com.julian.ricketmortypedia.common.base

/**
 * UIState represents a sealed class used to communicate a state event to the view by the viewModel.
 * It contains 4 states representing what the viewModel wants to notify the view with.
 */
sealed class UiState<out T> {
    object Init : UiState<Nothing>()
    data class Loading(val progress : Int? = null) : UiState<Nothing>()
    data class Error(val error : AppError) : UiState<Nothing>()
    data class Success<T>(val uiModel: T) : UiState<T>()
}