package com.julian.ricketmortypedia.common.base

/**
 * Result is a sealed class that allows to represents formatted data coming from a remote
 * source (i.e network or db)
 */
sealed class Result<out T> {
    data class Success<T>(
        val data: T
    ) : Result<T>()

    data class Error(
        val error: AppError
    ) : Result<Nothing>()
}