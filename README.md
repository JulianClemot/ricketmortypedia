# RickEtMortyPedia

L'application permet d'explorer les différents personnages de la série Rick & Morty.

La page d'accueil permet d'afficher une mosaïque des personnages et on peut lire leur nom, leur genre et leur statut actuel.
Un bouton permet d'afficher une BottomSheet afin de pouvoir filtrer parmi les résultats en fonction de leur genre et de leur statut.
Lorsque l'on scrolle, les nouveaux personnages sont chargés efficacement grâce à la librairie Paging V3 de Google.
Lors du clic sur l'un d'eux, on arrive alors sur la 2eme page, une page de détails qui affichent des informations détaillées sur le personnage sélectionné.

Le projet est structuré dans l'optique d'une architecture Clean (sans pour autant saturer le projet,
je n'ai pas créé de Use Cases par exemple car cela aurait compliqué encore plus le code pour une si petite application).
Néanmoins, dans le nommage des packages, j'ai privilégié le nommage par features plutôt que par layers, car c'est à mon gout plus clair pour s'y retrouver.
Je ne pense pas que ce soit une vérité absolue et chacun a ses gouts.

Le code utilise les librairies/patterns/fonctionnalités suivantes :

    - Apollo GraphQL
    - Coroutines
    - Flow
    - Paging V3
    - Le pattern MVVM
    - JetPack Navigation (en incluant également SafeArgs)
    - Palette
    - Hilt

Au niveau des build, l'app se décline en 2 build Types (debug et release) et 3 flavors (RickAndMorty, Rick, Morty).
Le build type debug permet d'afficher des logs et de pouvoir débugguer l'appli tandis que le build type release supprime tout ça.
Les 3 flavors permettent juste de bénéficier d'une icone customisée et d'un titre d'application différent.
Les 6 variants ont leur propre application Id ce qui permet de déployer sur le même téléphone sans
avoir de problèmes de signature ou d'override d'application (en cas de redéploiement pour changement de server par exemple).

Pour réaliser mes units tests, j'ai principalement utilisé Mockk couplé a Junit (4) ainsi que les librairies de testing des coroutines.
Le code coverage apparait comme insuffisant mais tous les viewModel/repositories/converters/utils sont testés.
Le reste correspond a des activity, fragment ou UIModel qui sont difficilement testables unitairement.
Le nombre s'explique par le faible nombre d'écran par rapport à l'architecture mise en place.
Tester unitairement la librairie de Paging est également une chose compliquée. J'aurais aimé mettre 
plus de tests en place notamment des Instrumented Tests permettant de tester les fragments pour 
augmenter le coverage mais le temps m'a manqué. Je serais ravi d'expliquer mes soucis rencontrés 
pendant un entretien technique.

![Code coverage](/assets/coverage.png)

Vous pourrez trouver les apk en debug des 3 flavors dans le dossier apk situé à la racine.

Time to get schwifty !