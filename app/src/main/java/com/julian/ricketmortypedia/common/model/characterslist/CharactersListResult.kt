package com.julian.ricketmortypedia.common.model.characterslist

data class CharactersListResult(
    val previousPage: Int?,
    val nextPage: Int?,
    val characters: List<CharacterLight>
)