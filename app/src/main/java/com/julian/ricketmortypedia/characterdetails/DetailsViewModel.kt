package com.julian.ricketmortypedia.characterdetails

import androidx.lifecycle.viewModelScope
import com.julian.ricketmortypedia.common.base.BaseViewModel
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
) : BaseViewModel<DetailsUiModel>() {
    var selectedColor: Int = 0
    lateinit var character: CharacterFull

    fun loadCharacter() {
        viewModelScope.launch {
            success(
                formatCharacterForView(character)
            )
        }
    }

    private fun formatCharacterForView(characterFull: CharacterFull) = DetailsUiModel(
        selectedColor,
        characterFull.name,
        characterFull.status.name.toLowerCase(Locale.getDefault())
            .capitalize(Locale.getDefault()),
        characterFull.gender.name.toLowerCase(Locale.getDefault())
            .capitalize(Locale.getDefault()),
        characterFull.avatarUrl,
        characterFull.species,
        characterFull.type,
        characterFull.location?.name.orEmpty(),
        characterFull.episodes.map { it.episode + " - " + it.name }
    )

}