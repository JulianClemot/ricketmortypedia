package com.julian.ricketmortypedia.common.data.pagination

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.julian.ricketmortypedia.common.base.Result
import com.julian.ricketmortypedia.common.model.QueryFilter
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.common.model.characterslist.CharactersListResult
import javax.inject.Inject

class CharactersListPagination @Inject constructor(private val functionToCall: suspend (Int, QueryFilter) -> Result<CharactersListResult>) :
    PagingSource<Int, CharacterLight>() {
    companion object {
        const val FIRST_PAGE_INDEX = 1
    }

    var queryFilter = QueryFilter()

    override val keyReuseSupported: Boolean
        get() = true

    override fun getRefreshKey(state: PagingState<Int, CharacterLight>): Int? {
        return state.anchorPosition
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CharacterLight> {
        try {
            val currentLoadingPageKey = params.key ?: FIRST_PAGE_INDEX
            return when (val response =
                functionToCall.invoke(currentLoadingPageKey, queryFilter)) {
                is Result.Error -> LoadResult.Error(response.error)
                is Result.Success -> {
                    return LoadResult.Page(
                        data = response.data.characters,
                        prevKey = response.data.previousPage,
                        nextKey = response.data.nextPage
                    )
                }
            }
        } catch (e: Exception) {
            return LoadResult.Error(e)
        }
    }
}