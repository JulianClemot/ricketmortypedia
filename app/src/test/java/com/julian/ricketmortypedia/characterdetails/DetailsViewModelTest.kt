package com.julian.ricketmortypedia.characterdetails

import com.julian.ricketmortypedia.common.base.UiState
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull
import com.julian.ricketmortypedia.common.model.characterdetails.Location
import com.julian.ricketmortypedia.utils.MainCoroutineScopeRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class DetailsViewModelTest {

    private lateinit var viewModel: DetailsViewModel

    @get:Rule
    val mainCoroutineScopeRule: MainCoroutineScopeRule = MainCoroutineScopeRule()

    @Before
    fun before() {
        viewModel = DetailsViewModel()
    }

    @Test
    fun `test loadCharacter should send Success UIState`() =
        mainCoroutineScopeRule.runBlockingTest {
            val character = CharacterFull(
                "1", "Toto", Gender.FEMALE, Status.ALIVE, "", "", "",
                Location("", "", "", ""), listOf()
            )
            val color = 2
            viewModel.character = character
            viewModel.selectedColor = color

            viewModel.loadCharacter()

            val result = viewModel.uiState.first()
            Assert.assertTrue(result is UiState.Success)
            val uiModel = (result as UiState.Success).uiModel
            Assert.assertEquals(character.name, uiModel.name)
            Assert.assertEquals(color, uiModel.selectedColor)
        }

}