package com.julian.ricketmortypedia.common.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch

/**
 * The BaseViewModel is an abstract class that defines a uiState SharedFlow that any implementation
 * of this class can listen to and react to UIState Events
 */
open class BaseViewModel<UI : BaseUiModel> : ViewModel() {

    protected val _uiState by lazy { MutableSharedFlow<UiState<UI>>(replay = 1) }

    val uiState: SharedFlow<UiState<UI>>
        get() = _uiState

    fun initScreen() {
        viewModelScope.launch {
            _uiState.emit(UiState.Init)
        }
    }

    suspend fun success(uiModel: UI) {
        _uiState.emit(UiState.Success(uiModel))
    }

    suspend fun error(appError: AppError) {
        _uiState.emit(UiState.Error(appError))
    }

    suspend fun loading(progress: Int? = null) {
        _uiState.emit(UiState.Loading(progress))
    }
}