package com.julian.ricketmortypedia.common.base

import com.julian.ricketmortypedia.utils.MainCoroutineScopeRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@ExperimentalCoroutinesApi
class BaseViewModelTest {

    private lateinit var baseViewModel: BaseViewModel<BaseUiModel>

    @get:Rule
    val mainCoroutineScopeRule: MainCoroutineScopeRule = MainCoroutineScopeRule()

    @Before
    fun before() {
        baseViewModel = BaseViewModel()
    }

    @Test
    fun `test initScreen should send Init UIState`() = mainCoroutineScopeRule.runBlockingTest {
        //function to test
        baseViewModel.initScreen()

        //result
        Assert.assertEquals(UiState.Init, baseViewModel.uiState.first())
    }

    @Test
    fun `test success should send Success UIState`() = mainCoroutineScopeRule.runBlockingTest {
        //parameters
        val uiModel1 = BaseUiModel()
        val uiModel2 = BaseUiModel()

        //function to test
        baseViewModel.success(uiModel1)

        //test on first result
        var result = baseViewModel.uiState.first()
        Assert.assertTrue(result is UiState.Success<BaseUiModel>)
        Assert.assertTrue(
            uiModel1 === (result as UiState.Success<BaseUiModel>).uiModel
        )

        //second test
        baseViewModel.success(uiModel2)

        //test on second result
        result = baseViewModel.uiState.first()
        Assert.assertTrue(result is UiState.Success<BaseUiModel>)
        Assert.assertTrue(
            uiModel2 === (result as UiState.Success<BaseUiModel>).uiModel
        )
    }

    @Test
    fun `test error should send error UIState`() = mainCoroutineScopeRule.runBlockingTest {
        //parameter
        val appError = AppError("")

        //function to test
        baseViewModel.error(appError)

        //result
        val result = baseViewModel.uiState.first()
        Assert.assertTrue(result is UiState.Error)
        Assert.assertEquals(
            appError, (result as UiState.Error).error
        )
    }

    @Test
    fun `test loading with a progress should send loading UIState`() =
        mainCoroutineScopeRule.runBlockingTest {
            //parameter
            val progress = 1

            //function to test
            baseViewModel.loading(1)

            //result
            val result = baseViewModel.uiState.first()
            Assert.assertTrue(result is UiState.Loading)
            Assert.assertEquals(
                progress, (result as UiState.Loading).progress
            )
        }

    @Test
    fun `test loading with a null progress should send loading UIState`() =
        mainCoroutineScopeRule.runBlockingTest {
            //second test with a null parameter
            baseViewModel.loading(null)

            //result
            val result = baseViewModel.uiState.first()
            Assert.assertTrue(result is UiState.Loading)
            Assert.assertNull((result as UiState.Loading).progress)
        }

}