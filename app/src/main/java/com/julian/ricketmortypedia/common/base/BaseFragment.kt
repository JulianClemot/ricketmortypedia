package com.julian.ricketmortypedia.common.base

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collect

/**
 * BaseFragment gives an abstraction of a fragment that can react to UIState events and retrieves its UIModels.
 */
abstract class BaseFragment<VM : BaseViewModel<UI>, B : ViewDataBinding, UI : BaseUiModel>(@LayoutRes val layoutId: Int) :
    Fragment() {
    val binding get() = _binding!!
    private var _binding: B? = null


    abstract val viewModel: VM

    // Coroutine listening for UI states
    private var uiStateJob: Job? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        _binding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uiStateJob = lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it) {
                    is UiState.Init -> onInit()
                    is UiState.Loading -> onLoading(it.progress)
                    is UiState.Error -> onError(it.error)
                    is UiState.Success -> onSuccess(it.uiModel)
                }
            }
        }

        viewModel.initScreen()
    }

    abstract fun onInit()

    open fun onLoading(progress: Int?) {
        //nothing to do
    }

    abstract fun onError(error: AppError)

    abstract fun onSuccess(uiModel: UI)

    fun handleError(appError: AppError) {
        //we could refine error handling based on error type if needed
        displayError(appError.message)
    }

    open fun displayError(message: String?) {
        Snackbar.make(binding.root, message.orEmpty(), Snackbar.LENGTH_LONG)
    }

    override fun onDestroyView() {
        _binding?.unbind()
        _binding = null
        super.onDestroyView()
    }

    override fun onStop() {
        super.onStop()
        uiStateJob?.cancel()
    }

    fun setActionBarColor(color: Int) {
        (activity as? AppCompatActivity)?.supportActionBar?.setBackgroundDrawable(
            ColorDrawable(
                color
            )
        )
    }
}