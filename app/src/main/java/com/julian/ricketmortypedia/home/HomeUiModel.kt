package com.julian.ricketmortypedia.home

import android.view.View
import androidx.paging.PagingData
import com.julian.ricketmortypedia.common.base.BaseUiModel
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull

sealed class HomeUiModel: BaseUiModel() {

    data class CharactersListModel(
        val charactersListPaginated: PagingData<CharacterLight>
    ) : HomeUiModel()

    data class DisplayCharacterDetailsModel(
        val avatarView: View,
        val selectedColor: Int,
        val characterFull: CharacterFull
    ) : HomeUiModel()
}