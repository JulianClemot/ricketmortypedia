package com.julian.ricketmortypedia.common.model

data class QueryFilter(var gender : Gender? = null, var status : Status? = null)
