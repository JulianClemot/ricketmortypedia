package com.julian.ricketmortypedia.common.model.converters

import com.julian.ricketmortypedia.common.model.characterdetails.CharacterDetailsResult
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterFull
import com.julian.ricketmortypedia.common.model.characterdetails.Episode
import com.julian.ricketmortypedia.common.model.characterdetails.Location
import com.julian.ricketmortypedia.ricketmortyapi.GetCharacterDetailsQuery

fun GetCharacterDetailsQuery.Data?.toModel() = CharacterDetailsResult(
    this?.character.toModel()
)

fun GetCharacterDetailsQuery.Character?.toModel() = CharacterFull(
    this?.id.orEmpty(),
    this?.name.orEmpty(),
    toGenderEnum(this?.gender),
    toStatusEnum(this?.status),
    this?.image,
    this?.species.orEmpty(),
    this?.type.orEmpty(),
    this?.location?.toModel(),
    this?.episode?.map { it.toModel() }.orEmpty()
)

fun GetCharacterDetailsQuery.Location?.toModel() = Location(
    this?.id.orEmpty(),
    this?.name.orEmpty(),
    this?.type.orEmpty(),
    this?.dimension.orEmpty()
)

fun GetCharacterDetailsQuery.Episode?.toModel() = Episode(
    this?.id.orEmpty(),
    this?.name.orEmpty(),
    this?.episode.orEmpty(),
    this?.air_date.orEmpty()
)