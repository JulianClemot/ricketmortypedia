package com.julian.ricketmortypedia.characterdetails.helpers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.julian.ricketmortypedia.databinding.ItemEpisodesBinding

class EpisodesAdapter(initialData: MutableList<String> = mutableListOf()) :
    RecyclerView.Adapter<EpisodesAdapter.EpisodeViewHolder>() {
    var items: MutableList<String> = initialData.toMutableList()
        set(value) {
            field.clear()
            field.addAll(value)
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder(
            ItemEpisodesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.count()

    class EpisodeViewHolder(val binding: ItemEpisodesBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(episodeName: String) {
            with(binding) {
                name.text = episodeName
            }
        }
    }
}