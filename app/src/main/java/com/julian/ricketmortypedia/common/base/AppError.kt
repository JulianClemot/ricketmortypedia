package com.julian.ricketmortypedia.common.base

/**
 * AppError is a class that is used to represent any error raised in the app.
 * It is implementing throwable because it allows us to throw it like an exception
 * to easily pass down the error to the upper-layers
 */
data class AppError(
    override val message: String? = "",
) : Throwable()