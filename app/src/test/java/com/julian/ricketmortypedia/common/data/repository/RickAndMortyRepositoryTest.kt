package com.julian.ricketmortypedia.common.data.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.apollographql.apollo.ApolloClient
import com.julian.ricketmortypedia.common.base.Result
import com.julian.ricketmortypedia.common.model.QueryFilter
import com.julian.ricketmortypedia.utils.MainCoroutineScopeRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.*
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
class RickAndMortyRepositoryTest {
    private val server = MockWebServer()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineScopeRule = MainCoroutineScopeRule()

    private lateinit var apolloClient: ApolloClient

    private lateinit var repository: RickAndMortyRepository

    @Before
    fun before() {
        server.start()
        server.url(SERVER_URL)
        apolloClient = ApolloClient.builder().serverUrl(server.url(SERVER_URL)).build()

        repository = RickAndMortyRepository(apolloClient, mainCoroutineScopeRule.dispatcher)
    }

    @After
    fun tearDown() {
        server.shutdown()
    }

    @Test
    fun `test loadCharacter with a success response should get a success result`() {
        server.enqueue(MockResponse().setBody(CHARACTERS_LIST_JSON))
        mainCoroutineScopeRule.dispatcher.runBlockingTest {
            val flow = repository.loadCharacters()
            val result = flow.first()
            Assert.assertNotNull(
                result
            )
        }
    }

    @Test
    fun `test getCharactersList with a success response should get a success result`() {
        server.enqueue(MockResponse().setBody(CHARACTERS_LIST_JSON))
        runBlocking {
            val result = repository.getCharactersList(0, QueryFilter())
            Assert.assertTrue(result is Result.Success)
            val expectedSizeOfCharacters = 20
            Assert.assertEquals(expectedSizeOfCharacters, (result as Result.Success).data.characters.size)
        }
    }

    @Test
    fun `test getCharactersList with an error response should get an error result`() {
        server.enqueue(MockResponse().setBody(ERROR_JSON))
        runBlocking {
            val result = repository.getCharactersList(0, QueryFilter())
            Assert.assertTrue(result is Result.Error)
            val expectedMessage = "404: Not Found"
            Assert.assertEquals(expectedMessage, (result as Result.Error).error.message)
        }
    }

    @Test
    fun `test GetCharacterDetails with a success response should get a success result`() {
        server.enqueue(MockResponse().setBody(GET_CHARACTERS_DETAILS_JSON))
        runBlocking {
            val result = repository.getCharacterDetails("")
            Assert.assertTrue(result is Result.Success)
            val expectedID = "1"
            Assert.assertEquals(expectedID, (result as Result.Success).data.character.id)
        }
    }

    @Test
    fun `test GetCharacterDetails with an error response should get an error result`() {
        server.enqueue(MockResponse().setBody(ERROR_JSON))
        runBlocking {
            val result = repository.getCharacterDetails("")
            Assert.assertTrue(result is Result.Error)
            val expectedMessage = "404: Not Found"
            Assert.assertEquals(expectedMessage, (result as Result.Error).error.message)
        }
    }

    companion object {
        const val SERVER_URL = "/"
        const val CHARACTERS_LIST_JSON =
            "{\"data\":{\"characters\":{\"__typename\":\"Characters\",\"info\":{\"__typename\":\"Info\",\"count\":671,\"pages\":34,\"next\":7,\"prev\":5},\"results\":[{\"__typename\":\"Character\",\"id\":\"101\",\"name\":\"E. Coli\",\"status\":\"Dead\",\"gender\":\"unknown\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/101.jpeg\"},{\"__typename\":\"Character\",\"id\":\"102\",\"name\":\"Donna Gueterman\",\"status\":\"Dead\",\"gender\":\"Female\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/102.jpeg\"},{\"__typename\":\"Character\",\"id\":\"103\",\"name\":\"Doofus Rick\",\"status\":\"unknown\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/103.jpeg\"},{\"__typename\":\"Character\",\"id\":\"104\",\"name\":\"Doom-Nomitron\",\"status\":\"Dead\",\"gender\":\"unknown\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/104.jpeg\"},{\"__typename\":\"Character\",\"id\":\"105\",\"name\":\"Dr. Glip-Glop\",\"status\":\"Dead\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/105.jpeg\"},{\"__typename\":\"Character\",\"id\":\"106\",\"name\":\"Dr. Schmidt\",\"status\":\"unknown\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/106.jpeg\"},{\"__typename\":\"Character\",\"id\":\"107\",\"name\":\"Dr. Wong\",\"status\":\"Alive\",\"gender\":\"Female\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/107.jpeg\"},{\"__typename\":\"Character\",\"id\":\"108\",\"name\":\"Dr. Xenon Bloom\",\"status\":\"Dead\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/108.jpeg\"},{\"__typename\":\"Character\",\"id\":\"109\",\"name\":\"Duck With Muscles\",\"status\":\"Dead\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/109.jpeg\"},{\"__typename\":\"Character\",\"id\":\"110\",\"name\":\"Eli\",\"status\":\"Alive\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/110.jpeg\"},{\"__typename\":\"Character\",\"id\":\"111\",\"name\":\"Eli's Girlfriend\",\"status\":\"Alive\",\"gender\":\"Female\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/111.jpeg\"},{\"__typename\":\"Character\",\"id\":\"112\",\"name\":\"Eric McMan\",\"status\":\"Alive\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/112.jpeg\"},{\"__typename\":\"Character\",\"id\":\"113\",\"name\":\"Eric Stoltz Mask Morty\",\"status\":\"unknown\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/113.jpeg\"},{\"__typename\":\"Character\",\"id\":\"114\",\"name\":\"Ethan\",\"status\":\"unknown\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/114.jpeg\"},{\"__typename\":\"Character\",\"id\":\"115\",\"name\":\"Ethan\",\"status\":\"Alive\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/115.jpeg\"},{\"__typename\":\"Character\",\"id\":\"116\",\"name\":\"Evil Beth Clone\",\"status\":\"Dead\",\"gender\":\"Female\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/116.jpeg\"},{\"__typename\":\"Character\",\"id\":\"117\",\"name\":\"Evil Jerry Clone\",\"status\":\"Dead\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/117.jpeg\"},{\"__typename\":\"Character\",\"id\":\"118\",\"name\":\"Evil Morty\",\"status\":\"Alive\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/118.jpeg\"},{\"__typename\":\"Character\",\"id\":\"119\",\"name\":\"Evil Rick\",\"status\":\"Dead\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/119.jpeg\"},{\"__typename\":\"Character\",\"id\":\"120\",\"name\":\"Evil Summer Clone\",\"status\":\"Dead\",\"gender\":\"Female\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/120.jpeg\"}]}}}"
        const val GET_CHARACTERS_DETAILS_JSON =
            "{\"data\":{\"character\":{\"__typename\":\"Character\",\"id\":\"1\",\"name\":\"Rick Sanchez\",\"status\":\"Alive\",\"location\":{\"__typename\":\"Location\",\"id\":\"20\",\"name\":\"Earth (Replacement Dimension)\",\"type\":\"Planet\",\"dimension\":\"Replacement Dimension\"},\"episode\":[{\"__typename\":\"Episode\",\"id\":\"1\",\"name\":\"Pilot\",\"episode\":\"S01E01\",\"air_date\":\"December 2, 2013\"},{\"__typename\":\"Episode\",\"id\":\"2\",\"name\":\"Lawnmower Dog\",\"episode\":\"S01E02\",\"air_date\":\"December 9, 2013\"},{\"__typename\":\"Episode\",\"id\":\"3\",\"name\":\"Anatomy Park\",\"episode\":\"S01E03\",\"air_date\":\"December 16, 2013\"},{\"__typename\":\"Episode\",\"id\":\"4\",\"name\":\"M. Night Shaym-Aliens!\",\"episode\":\"S01E04\",\"air_date\":\"January 13, 2014\"},{\"__typename\":\"Episode\",\"id\":\"5\",\"name\":\"Meeseeks and Destroy\",\"episode\":\"S01E05\",\"air_date\":\"January 20, 2014\"},{\"__typename\":\"Episode\",\"id\":\"6\",\"name\":\"Rick Potion #9\",\"episode\":\"S01E06\",\"air_date\":\"January 27, 2014\"},{\"__typename\":\"Episode\",\"id\":\"7\",\"name\":\"Raising Gazorpazorp\",\"episode\":\"S01E07\",\"air_date\":\"March 10, 2014\"},{\"__typename\":\"Episode\",\"id\":\"8\",\"name\":\"Rixty Minutes\",\"episode\":\"S01E08\",\"air_date\":\"March 17, 2014\"},{\"__typename\":\"Episode\",\"id\":\"9\",\"name\":\"Something Ricked This Way Comes\",\"episode\":\"S01E09\",\"air_date\":\"March 24, 2014\"},{\"__typename\":\"Episode\",\"id\":\"10\",\"name\":\"Close Rick-counters of the Rick Kind\",\"episode\":\"S01E10\",\"air_date\":\"April 7, 2014\"},{\"__typename\":\"Episode\",\"id\":\"11\",\"name\":\"Ricksy Business\",\"episode\":\"S01E11\",\"air_date\":\"April 14, 2014\"},{\"__typename\":\"Episode\",\"id\":\"12\",\"name\":\"A Rickle in Time\",\"episode\":\"S02E01\",\"air_date\":\"July 26, 2015\"},{\"__typename\":\"Episode\",\"id\":\"13\",\"name\":\"Mortynight Run\",\"episode\":\"S02E02\",\"air_date\":\"August 2, 2015\"},{\"__typename\":\"Episode\",\"id\":\"14\",\"name\":\"Auto Erotic Assimilation\",\"episode\":\"S02E03\",\"air_date\":\"August 9, 2015\"},{\"__typename\":\"Episode\",\"id\":\"15\",\"name\":\"Total Rickall\",\"episode\":\"S02E04\",\"air_date\":\"August 16, 2015\"},{\"__typename\":\"Episode\",\"id\":\"16\",\"name\":\"Get Schwifty\",\"episode\":\"S02E05\",\"air_date\":\"August 23, 2015\"},{\"__typename\":\"Episode\",\"id\":\"17\",\"name\":\"The Ricks Must Be Crazy\",\"episode\":\"S02E06\",\"air_date\":\"August 30, 2015\"},{\"__typename\":\"Episode\",\"id\":\"18\",\"name\":\"Big Trouble in Little Sanchez\",\"episode\":\"S02E07\",\"air_date\":\"September 13, 2015\"},{\"__typename\":\"Episode\",\"id\":\"19\",\"name\":\"Interdimensional Cable 2: Tempting Fate\",\"episode\":\"S02E08\",\"air_date\":\"September 20, 2015\"},{\"__typename\":\"Episode\",\"id\":\"20\",\"name\":\"Look Who's Purging Now\",\"episode\":\"S02E09\",\"air_date\":\"September 27, 2015\"},{\"__typename\":\"Episode\",\"id\":\"21\",\"name\":\"The Wedding Squanchers\",\"episode\":\"S02E10\",\"air_date\":\"October 4, 2015\"},{\"__typename\":\"Episode\",\"id\":\"22\",\"name\":\"The Rickshank Rickdemption\",\"episode\":\"S03E01\",\"air_date\":\"April 1, 2017\"},{\"__typename\":\"Episode\",\"id\":\"23\",\"name\":\"Rickmancing the Stone\",\"episode\":\"S03E02\",\"air_date\":\"July 30, 2017\"},{\"__typename\":\"Episode\",\"id\":\"24\",\"name\":\"Pickle Rick\",\"episode\":\"S03E03\",\"air_date\":\"August 6, 2017\"},{\"__typename\":\"Episode\",\"id\":\"25\",\"name\":\"Vindicators 3: The Return of Worldender\",\"episode\":\"S03E04\",\"air_date\":\"August 13, 2017\"},{\"__typename\":\"Episode\",\"id\":\"26\",\"name\":\"The Whirly Dirly Conspiracy\",\"episode\":\"S03E05\",\"air_date\":\"August 20, 2017\"},{\"__typename\":\"Episode\",\"id\":\"27\",\"name\":\"Rest and Ricklaxation\",\"episode\":\"S03E06\",\"air_date\":\"August 27, 2017\"},{\"__typename\":\"Episode\",\"id\":\"28\",\"name\":\"The Ricklantis Mixup\",\"episode\":\"S03E07\",\"air_date\":\"September 10, 2017\"},{\"__typename\":\"Episode\",\"id\":\"29\",\"name\":\"Morty's Mind Blowers\",\"episode\":\"S03E08\",\"air_date\":\"September 17, 2017\"},{\"__typename\":\"Episode\",\"id\":\"30\",\"name\":\"The ABC's of Beth\",\"episode\":\"S03E09\",\"air_date\":\"September 24, 2017\"},{\"__typename\":\"Episode\",\"id\":\"31\",\"name\":\"The Rickchurian Mortydate\",\"episode\":\"S03E10\",\"air_date\":\"October 1, 2017\"},{\"__typename\":\"Episode\",\"id\":\"32\",\"name\":\"Edge of Tomorty: Rick, Die, Rickpeat\",\"episode\":\"S04E01\",\"air_date\":\"November 10, 2019\"},{\"__typename\":\"Episode\",\"id\":\"33\",\"name\":\"The Old Man and the Seat\",\"episode\":\"S04E02\",\"air_date\":\"November 17, 2019\"},{\"__typename\":\"Episode\",\"id\":\"34\",\"name\":\"One Crew Over the Crewcoo's Morty\",\"episode\":\"S04E03\",\"air_date\":\"November 24, 2019\"},{\"__typename\":\"Episode\",\"id\":\"35\",\"name\":\"Claw and Hoarder: Special Ricktim's Morty\",\"episode\":\"S04E04\",\"air_date\":\"December 8, 2019\"},{\"__typename\":\"Episode\",\"id\":\"36\",\"name\":\"Rattlestar Ricklactica\",\"episode\":\"S04E05\",\"air_date\":\"December 15, 2019\"},{\"__typename\":\"Episode\",\"id\":\"37\",\"name\":\"Never Ricking Morty\",\"episode\":\"S04E06\",\"air_date\":\"May 3, 2020\"},{\"__typename\":\"Episode\",\"id\":\"38\",\"name\":\"Promortyus\",\"episode\":\"S04E07\",\"air_date\":\"May 10, 2020\"},{\"__typename\":\"Episode\",\"id\":\"39\",\"name\":\"The Vat of Acid Episode\",\"episode\":\"S04E08\",\"air_date\":\"May 17, 2020\"},{\"__typename\":\"Episode\",\"id\":\"40\",\"name\":\"Childrick of Mort\",\"episode\":\"S04E09\",\"air_date\":\"May 24, 2020\"},{\"__typename\":\"Episode\",\"id\":\"41\",\"name\":\"Star Mort: Rickturn of the Jerri\",\"episode\":\"S04E10\",\"air_date\":\"May 31, 2020\"}],\"species\":\"Human\",\"type\":\"\",\"gender\":\"Male\",\"image\":\"https://rickandmortyapi.com/api/character/avatar/1.jpeg\"}}}"
        const val ERROR_JSON =
            "{\"errors\":[{\"message\":\"404: Not Found\",\"locations\":[{\"line\":1,\"column\":71}],\"path\":[\"characters\"],\"extensions\":{\"code\":\"INTERNAL_SERVER_ERROR\",\"response\":{\"url\":\"http://localhost:8080/api/character/?page=1000\",\"status\":404,\"statusText\":\"Not Found\",\"body\":{\"error\":\"There is nothing here\"}}}}],\"data\":{\"characters\":null}}"
    }
}