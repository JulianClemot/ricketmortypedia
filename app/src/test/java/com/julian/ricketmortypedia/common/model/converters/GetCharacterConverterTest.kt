package com.julian.ricketmortypedia.common.model.converters

import com.julian.ricketmortypedia.ricketmortyapi.GetCharacterDetailsQuery
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetCharacterConverterTest {
    @MockK(relaxed = true)
    private lateinit var character: GetCharacterDetailsQuery.Character

    @MockK(relaxed = true)
    private lateinit var data: GetCharacterDetailsQuery.Data

    @MockK(relaxed = true)
    private lateinit var episode: GetCharacterDetailsQuery.Episode

    @MockK(relaxed = true)
    private lateinit var location: GetCharacterDetailsQuery.Location

    @Before
    fun before() {
        MockKAnnotations.init(this)
        initEpisode()
        initLocation()
        initCharacter()
        initData()
    }

    @Test
    fun `test episode toModel sets appropriate values`() {
        val episodeModel = episode.toModel()
        Assert.assertEquals(episodeModel.id, episode.id)
    }

    @Test
    fun `test location toModel sets appropriate values`() {
        val locationModel = location.toModel()
        Assert.assertEquals(locationModel.id, location.id)
    }

    @Test
    fun `test character toModel sets appropriate values`() {
        val characterModel = character.toModel()
        Assert.assertEquals(characterModel.id, character.id)
        Assert.assertEquals(characterModel.episodes.first().id, episode.id)
        Assert.assertEquals(characterModel.location?.id, location.id)
    }

    @Test
    fun `test data toModel sets appropriate values`() {
        val characterDetailsResult = data.toModel()
        Assert.assertEquals(characterDetailsResult.character.id, character.id)
    }

    private fun initData() {
        every { data.character } returns character
    }

    private fun initCharacter() {
        every { character.id } returns "character"
        every { character.name } returns "name"
        every { character.gender } returns "Male"
        every { character.status } returns "Alive"
        every { character.type } returns "type"
        every { character.episode } returns listOf(episode)
        every { character.location } returns location
    }

    private fun initEpisode() {
        every { episode.id } returns "episode"
    }

    private fun initLocation() {
        every { location.id } returns "location"
    }
}