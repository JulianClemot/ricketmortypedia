package com.julian.ricketmortypedia.common.model.converters

import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.ricketmortyapi.GetCharactersQuery
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetCharactersConverterTest {
    @MockK(relaxed = true)
    private lateinit var characters: GetCharactersQuery.Characters

    @Before
    fun before() {
        MockKAnnotations.init(this)
        initCharacters()
    }

    @Test
    fun `test toGenderEnum with known value sets appropriate enum`() {
        var stringEnum = "Male"
        var enumToTest = toGenderEnum(stringEnum)
        Assert.assertEquals(Gender.MALE, enumToTest)
        stringEnum = "MALE"
        enumToTest = toGenderEnum(stringEnum)
        Assert.assertEquals(Gender.MALE, enumToTest)
        stringEnum = "male"
        enumToTest = toGenderEnum(stringEnum)
        Assert.assertEquals(Gender.MALE, enumToTest)
    }

    @Test
    fun `test toGenderEnum with corrupted value sets default Unknown enum`() {
        val stringEnum = "sdcvdcds"
        val enumToTest = toGenderEnum(stringEnum)
        Assert.assertEquals(Gender.UNKNOWN, enumToTest)
    }

    @Test
    fun `test toStatusEnum with known value sets appropriate enum`() {
        var stringEnum = "Alive"
        var enumToTest = toStatusEnum(stringEnum)
        Assert.assertEquals(Status.ALIVE, enumToTest)
        stringEnum = "ALIVE"
        enumToTest = toStatusEnum(stringEnum)
        Assert.assertEquals(Status.ALIVE, enumToTest)
        stringEnum = "alive"
        enumToTest = toStatusEnum(stringEnum)
        Assert.assertEquals(Status.ALIVE, enumToTest)
    }

    @Test
    fun `test toStatusEnum with corrupted value sets default Unknown enum`() {
        val stringEnum = "sdcvdcds"
        val enumToTest = toStatusEnum(stringEnum)
        Assert.assertEquals(Status.UNKNOWN, enumToTest)
    }

    @Test
    fun `test Result toModel with good value creates a CharacterLight`() {
        val apiResult = characters.results?.first()
        val model = apiResult.toModel()
        Assert.assertNotNull(model)
        Assert.assertEquals(apiResult?.id, model.id)
        Assert.assertEquals(apiResult?.name, model.name)
    }

    @Test
    fun `test GetCharactersQuery Characters toModel with good value creates a CharacterLight`() {
        val model = characters.toModel()
        Assert.assertNotNull(model)
        Assert.assertEquals(characters.info?.prev, model.previousPage)
        Assert.assertEquals(characters.info?.next, model.nextPage)
        Assert.assertEquals(characters.results?.size, model.characters.size)
    }

    private fun initCharacters() {
        every { characters.results } returns listOf(
            GetCharactersQuery.Result(id = "1", name = "a", status = "", gender = "", image = ""),
            GetCharactersQuery.Result(id = "2", name = "b", status = "", gender = "", image = ""),
            GetCharactersQuery.Result(id = "3", name = "c", status = "", gender = "", image = ""),
        )
        every { characters.info } returns GetCharactersQuery.Info(
            count = 2,
            pages = 10,
            next = 2,
            prev = 0
        )
    }
}