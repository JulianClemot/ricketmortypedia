package com.julian.ricketmortypedia.common.data.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Input
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.coroutines.await
import com.julian.ricketmortypedia.common.base.AppError
import com.julian.ricketmortypedia.common.base.Result
import com.julian.ricketmortypedia.common.data.pagination.CharactersListPagination
import com.julian.ricketmortypedia.common.model.Gender
import com.julian.ricketmortypedia.common.model.QueryFilter
import com.julian.ricketmortypedia.common.model.Status
import com.julian.ricketmortypedia.common.model.characterdetails.CharacterDetailsResult
import com.julian.ricketmortypedia.common.model.characterslist.CharacterLight
import com.julian.ricketmortypedia.common.model.characterslist.CharactersListResult
import com.julian.ricketmortypedia.common.model.converters.toModel
import com.julian.ricketmortypedia.ricketmortyapi.GetCharacterDetailsQuery
import com.julian.ricketmortypedia.ricketmortyapi.GetCharactersQuery
import com.julian.ricketmortypedia.ricketmortyapi.type.FilterCharacter
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class RickAndMortyRepository @Inject constructor(
    private val apolloClient: ApolloClient,
    private val dispatcher: CoroutineDispatcher,
) {
    val charactersListPagination: CharactersListPagination =
        CharactersListPagination { page, filter ->
            getCharactersList(
                page,
                filter
            )
        }

    fun loadCharacters(
        filterGender: Gender? = charactersListPagination.queryFilter.gender,
        filterStatus: Status? = charactersListPagination.queryFilter.status
    ): Flow<PagingData<CharacterLight>> {
        charactersListPagination.queryFilter.apply {
            gender = filterGender
            status = filterStatus
        }
        return paginationFlow()
    }

    suspend fun getCharactersList(page: Int, filter: QueryFilter): Result<CharactersListResult> =
        processCharactersResponse(
            apolloClient.query(
                GetCharactersQuery(
                    Input.fromNullable(page),
                    Input.fromNullable(
                        FilterCharacter(
                            gender = Input.fromNullable(filter.gender?.name),
                            status = Input.fromNullable(filter.status?.name)
                        )
                    )
                )
            ).await()
        )


    private fun paginationFlow() = Pager(PagingConfig(pageSize = 20)) {
        charactersListPagination
    }.flow.flowOn(dispatcher)


    private fun processCharactersResponse(response: Response<GetCharactersQuery.Data>): Result<CharactersListResult> {
        return if (response.hasErrors() || response.data?.characters == null) {
            Result.Error(AppError(response.errors?.first()?.message))
        } else {
            //I wish we could avoid using !! to prevent getting an optional here. It's due to the
            // fact that the compiler cannot smart cast because the object is defined in another module
            Result.Success(response.data!!.characters.toModel())
        }
    }

    suspend fun getCharacterDetails(id: String): Result<CharacterDetailsResult> =
        processCharacterDetailsResponse(apolloClient.query(GetCharacterDetailsQuery(id)).await())

    private fun processCharacterDetailsResponse(response: Response<GetCharacterDetailsQuery.Data>): Result<CharacterDetailsResult> {
        return if (response.hasErrors() || response.data == null) {
            Result.Error(AppError(response.errors?.first()?.message))
        } else {
            Result.Success(response.data.toModel())
        }
    }
}