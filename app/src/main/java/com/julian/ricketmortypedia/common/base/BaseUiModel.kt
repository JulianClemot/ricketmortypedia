package com.julian.ricketmortypedia.common.base

/**
 * BaseUIModel is just an empty class to operate as a guide for the generic part of the
 * BaseViewModel in order for it to be able structure sent data
 */
open class BaseUiModel