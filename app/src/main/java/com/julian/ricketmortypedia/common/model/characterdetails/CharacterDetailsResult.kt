package com.julian.ricketmortypedia.common.model.characterdetails

data class CharacterDetailsResult(
    val character: CharacterFull
)