package com.julian.ricketmortypedia.common.application

import android.app.Application
import com.julian.ricketmortypedia.BuildConfig
import com.julian.ricketmortypedia.common.base.utils.ReleaseTree
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class AppApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initDebugConfiguration()
    }

    private fun initDebugConfiguration() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Timber.i("Enabling Debug")
        } else {
            Timber.plant(ReleaseTree())
        }
    }

}