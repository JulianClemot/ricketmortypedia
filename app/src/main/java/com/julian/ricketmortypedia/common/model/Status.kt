package com.julian.ricketmortypedia.common.model

enum class Status {
    ALIVE,
    DEAD,
    UNKNOWN,
}