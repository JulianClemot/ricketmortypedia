package com.julian.ricketmortypedia.common.model.characterdetails

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Episode(
    val id: String,
    val name: String,
    val episode: String,
    val airDate: String
) : Parcelable